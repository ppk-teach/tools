% Web application
% Piyush P Kurur
% Sept 29, 2016


# Web applications

## Components of a web application


1. The browser

2. The server (essentially state less)

3. The persistence layer (Database)

4. Authentication

5. Authorisation


## State in a REST session: Cookie

1. Keep track of small amount of data across state

2. Server sends a cookie to the browser when it connects to the site

3. The browser is supposed to send it back when it connects again.

```

HTTP/1.0 200 OK
Set-Cookie: lu=Rg3vHJZnehYLjVg7qi3bZjzg;
	Expires=Tue, 15 Jan 2013 21:47:38 GMT;
	Path=/; Domain=.example.com; HttpOnly

Set-Cookie: made_write_conn=1295214458;
	Path=/; Domain=.example.com

Set-Cookie: reg_fb_gate=deleted; Expires=Thu, 01 Jan 1970 00:00:01 GMT;
	Path=/; Domain=.example.com; HttpOnly

```


## REST based Authentication: Example

1. GET /login/ : Gives a form

2. POST /login/ with two parameters `username` and `password` should authenticate

3. Store the authenticated user in the cookie.

One can use something like:

```
Set-Cookie: session_user=knuth
```

## Security considerations on server side

1. Cookie should be tamper proof: Encrypted authentication

2. Man in the Middle should not be able to use cookie: Https

## CSRF attack

Consider the following browser session

1. You are using Internet banking `bank.com`

2. You are browsing `http://example.com/devil/blog`

3. Devil's blog has Javasript that posts to bank.com/transfer-funds


## Form action.

Forms have two REST action

* GET formurl
* POST formurl


## How to prevent CSRF

1. Form url will have a hidden field which contains the csrf token

2. A post without the csrf token field is rejected

3. token can be verified with the cookie data.

4. Same page policy


## Other attacks

1. XSS: cross site scripting attack.

2. Sql injection.
